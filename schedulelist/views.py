from django.shortcuts import render, redirect

from .models import Kegiatan


def index(request):
    list_kegiatan = Kegiatan.objects.all()
    content = {
        "Kegiatan": list_kegiatan
    }

    return render(request, 'jadwal/index.html', content)


def tambah(request):
    data_kegiatan = request.POST.get('kegiatan')
    data_tempat = request.POST.get('tempat')
    data_tanggal = request.POST.get('tanggal')
    data_waktu = request.POST.get('waktu')
    data_kategori = request.POST.get('kategori')
    kegiatan_baru = Kegiatan(nama_kegiatan=data_kegiatan,
                             tempat=data_tempat, tanggal=data_tanggal, waktu=data_waktu, kategori=data_kategori)
    kegiatan_baru.save()
    return redirect('schedulelist:kegiatan')


def deleteItem(request, Kegiatan_id):
    item = Kegiatan.objects.get(id=Kegiatan_id)
    item.delete()
    return redirect('schedulelist:kegiatan')
