from django.apps import AppConfig


class SchedulelistConfig(AppConfig):
    name = 'schedulelist'
