from django.urls import path

from . import views

app_name = 'schedulelist'

urlpatterns = [
    path('', views.index, name="kegiatan"),
    path('submit/', views.tambah, name="submit"),
    path('delete/<int:Kegiatan_id>/', views.deleteItem, name="delete")
]
