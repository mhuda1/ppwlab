from django.db import models
from django.utils import timezone
from datetime import datetime, date


class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=20)
    tempat = models.CharField(max_length=20)
    tanggal = models.DateField('tanggal')
    waktu = models.TimeField('jam')
    kategori = models.CharField(max_length=20)

    def __str__(self):
        return self.nama_kegiatan
