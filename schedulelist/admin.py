from django.contrib import admin
from .models import Kegiatan

admin.site.site_header = "Huda Backend"
admin.site.site_title = "Huda Admin Area"
admin.site.index_title = "Selamat Datang cuk"


class KegiatanAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Nama Kegiatan', {"fields": ['nama_kegiatan']}),
        ('Tempat', {"fields": ['tempat']}),
        ('Tanggal', {"fields": ['tanggal']}),
        ('Waktu', {"fields": ['waktu']}),
        ('Kategori', {"fields": ['kategori']}),
    ]


admin.site.register(Kegiatan, KegiatanAdmin)
